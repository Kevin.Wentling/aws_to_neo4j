import json
import logging
import re

def load_json(filename):
    """Load a JSON file from disk."""
    result = {}
    with open(filename, 'r') as f:
        try:
            result = json.load(f)
        except json.decoder.JSONDecodeError:
            logging.info('Failed to JSON decode {}'.format(filename))
    return result

def _find_keys(node, kv):
    """Go through an object recursively to find all keys."""
    if isinstance(node, list):
        for i in node:
            for x in _find_keys(i, kv):
               yield x
    elif isinstance(node, dict):
        if kv in node:
            yield node[kv]
        for j in node.values():
            for x in _find_keys(j, kv):
                yield x

def find_keys(node, kv):
    """Find all occurences of key 'kv' in object node."""
    result = list(_find_keys(node, kv))
    return result

def flatten_data(y):
    """Flatten a dict to 1 level."""
    out = {}

    def flatten(x, name=''):
        if type(x) is dict:
            for a in x:
                flatten(x[a], name + a + '_')
        elif type(x) is list:
            i = 0
            for a in x:
                flatten(a, name + str(i) + '_')
                i += 1
        else:
            out[name[:-1]] = x

    flatten(y)
    return out

def consolidate_flattened_data(object, pattern, key, value):
    """
    Recreate a list that was destroyed during the flattening process
    and set keys and values flat in the dict.
    """
    if not isinstance(object, dict):
        return None

    result = {}

    _finds = []

    for k, v in object.items():
        if pattern.lower() in k.lower() and key.lower() in k.lower():
            _finds.append({k:v})
        if pattern.lower() in k.lower() and value.lower() in k.lower():
            _finds.append({k:v})

    _keys = []
    _values = []
    for _find in _finds:
        for k, v in _find.items():
            for part in k.split('_'):
                if re.match(r'^\d+$', part):
                    if key.lower() in k.lower():
                        _keys.insert(int(part), v)
                    if value.lower() in k.lower():
                        _values.insert(int(part), v)

    if len(_keys) == len(_values):
        for index, value in enumerate(_keys):
            result.update({value: _values[index]})

    return result

def remove_keys_from_dict_fuzzy(object, keys):
    """Remove key from dict using a regex pattern."""
    _object = object.copy()
    if not isinstance(object, dict):
        return None
    for item in object:
        if re.match(r'^{}'.format(keys), item):
            _object.pop(item)

    return _object

def merge_dicts(dict_a, dict_b):
    return {**dict_a, **dict_b}

def remove_key_from_dict(object, key):
    """Remove a provided key from a dict."""
    object.pop(key, None)
    return object

def find_keys_with_pattern(data, pattern):
    """Loop through a dict for a key that matches a regex pattern."""

    if not pattern:
        return
    result = []
    if isinstance(data, dict):
        for k,v in data.items():
            if isinstance(v, dict):
                _result = find_keys_with_pattern(v, pattern)
                result += _result
            elif isinstance(v, list):
                for a in v:
                    _result = find_keys_with_pattern(a, pattern)
                    result += _result
            else:
                if re.match(r'{}'.format(pattern), k):
                    result.append({k: v})
    elif isinstance(data, list):
        for a in data:
            find_keys_with_pattern(a, pattern)

    return result
