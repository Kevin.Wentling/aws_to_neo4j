import sys
import json
import argparse
import re
import threading

# Disable debugging logging from the Neo4j module
#import logging
#logging.getLogger('neo4j').setLevel(logging.INFO)


from libs.helpers.helpers import (
    load_json, flatten_data, consolidate_flattened_data, remove_keys_from_dict_fuzzy, merge_dicts,
    find_keys, find_keys_with_pattern
)
from helpers import Neo4jConnector, create_node, create_relationships_from_links
from libs.logger import logger
from datetime import datetime
import helpers

#logger.disable()

log = logger.getLogger(__name__)

def loadConfig(filename=None):

    if not filename:
        filename = 'config/config.yaml'

    log.info('Loading settings from {}'.format(filename))

    settings = helpers.parseYaml(filename)

    return settings

def formatResourceData(resource, resource_name_singular):
    tags = consolidate_flattened_data(resource, 'Tags', 'Key', 'Value')
    result = remove_keys_from_dict_fuzzy(resource, 'Tags')

    result = merge_dicts(result, tags)

    result = helpers.create_resource_alias(result, '{}Id'.format(resource_name_singular), 'id')

    return result

def extractResourceLinks(resource):
    return find_keys_with_pattern(resource, '.*Id$')


def linkCleaner(resource_id, links, resource_name_singular, config):
    _resource_links = []
    for link in links:
        _link = helpers.link_normalizer(link, resource_name_singular, config)
        if _link and _link not in _resource_links:
            for k, v in _link.items():
                # Remove links to self
                if v == resource_id:
                    continue
                # Ignore resources in config ignore list
                elif 'resource' in config and 'ignore' in config['resource'] and k not in config['resource']['ignore']:
                    _resource_links.append(_link)
                elif 'resource' not in config or 'ignore' not in config['resource']:
                    _resource_links.append(_link)

    return _resource_links

def resourceCleaner(resource, resource_name_singular, config):

    resource['links'] = linkCleaner(resource['id'], resource['links'], resource_name_singular, config)

    if 'resource' in config and 'ignore' in config['resource'] and resource_name_singular in config['resource']['ignore']:
        return None

    return resource

def parseFile(config, filename, accountid):

    item = load_json(filename)

    metadata = {}

    metadata.update({'accountid': accountid})
    metadata.update({'lastSeen': datetime.today().strftime('%Y-%m-%d')})

    module = (filename.split('/')[-1]).split('_')[1]
    metadata.update({'module': module})

    if 'region' in item:
        metadata.update({'region': item['region']})

    resource_list = []

    resource_name, resource_name_singular = helpers.get_resource_name(filename)
    _results = find_keys(item, resource_name)

    resource_id_alias = resource_name_singular
    if 'resource' in config and 'convertion' in config['resource']:
        for convertion in config['resource']['convertion']:
            if convertion['resource'] == resource_name_singular:
                resource_id_alias = convertion['alias']

    for resources in _results:
        for resource in resources:

            resource = flatten_data(resource)
            resource = formatResourceData(resource, resource_id_alias)

            resource_links = extractResourceLinks(resource)

            log.debug(resource_links)

            resource = helpers.add_metadata_to_resource(config, resource, metadata)

            resource_id = resource_name_singular
            if 'id' in resource:
                resource_id = resource['id']

            log.debug(resource)


            resource_list.append({
                'name': resource_name_singular,
                'id': resource_id,
                'resource': resource,
                'links': resource_links})

    resource_list = [resource for resource in resource_list if resourceCleaner(resource, resource_name_singular, config)]

    return resource_list

def insert_into_neo4j(nc, resource):
    create_node(nc, resource['name'], resource['id'], resource['resource'])
    create_relationships_from_links(nc, resource['name'], resource['id'], resource['links'])

def insertToNeo4j(resource_list, filename):

    nc = Neo4jConnector("10.97.147.239",
                        "neo4j",
                        "NmG0WggcCA3vihYirmgjETvV4JqaVvSDLaL1XdFAnINrSwyycEHSwbSQD75EKvAn",
                        tls=True,
                        selfsigned=True)

    resource_count = 0
    try:
        for resource in resource_list:
            resource_count += 1

            log.info(f'{filename}: Working on resource {resource_count}/{len(resource_list)}')

            if args.noop:
                print('Resource: {}, {}, {}'.format(resource['name'], resource['id'], resource['resource']))
                print('Links: {}'.format(resource['links']))
            else:
                create_node(nc, resource['name'], resource['id'], resource['resource'])
                create_relationships_from_links(nc, resource['name'], resource['id'], resource['links'])
    finally:
        nc.close()

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Ingest AWS data into Neo4j database.')
    fileGroup = parser.add_mutually_exclusive_group(required=True)
    fileGroup.add_argument('--filename', '-f', help='File to parse.')
    fileGroup.add_argument('--directory', '-d', help='Directory of files to parse.')
    parser.add_argument('--accountid', '-a',
                        help='AWS resource account ID.')
    parser.add_argument('--config', '-c',
                        help='Alternative configuration file.')
    parser.add_argument('--noop', action='store_true',
                        help='no-op/dry-run.')
    parser.add_argument('--loglevel', '-l', help='Change log level.')
    args = parser.parse_args()

    if args.filename and not args.accountid:
        log.error('Owner ID information not provided.')
        sys.exit(1)

    if args.accountid:
        if not re.match('^[0-9]{12}$', args.accountid):
            log.error('Invalid owner id provided.')
            sys.exit(1)

    config = loadConfig(args.config)

    if args.loglevel:
        if (args.loglevel == 'debug' or
            args.loglevel == 'info' or
            args.loglevel == 'warning' or
            args.loglevel == 'critical'):
            logger.configure(level=args.loglevel)
        else:
            print(f'Invalid log level {args.loglevel}')
            print('Allowed values: debug, info, warning and critical.')
            sys.exit(1)

    if args.filename:
        log.info('Parsing file {}'.format(args.filename))
        resource_list = parseFile(config, args.filename, args.accountid)
        insertToNeo4j(resource_list, args.filename)
    elif args.directory:
        log.info('Parsing files in directory {}'.format(args.directory))
        files = helpers.get_json_files_in_dir(config, args.directory)
        log.debug('Going to parse files: {}'.format(files))
        i = 0
        for file in files:
            i += 1
            log.info('Parsing file {} {}/{}'.format(file, i, len(files)))
            #log.info('Parsing file {}'.format(file))
            resource_list = parseFile(config, file, args.accountid)
            insertToNeo4j(resource_list, file)
