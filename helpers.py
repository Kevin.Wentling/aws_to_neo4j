"""aws_to_neo4j specific helper functions."""

from dataclasses import asdict, dataclass
import re
import yaml
import threading

from neo4j import GraphDatabase
from os.path import isfile, join, abspath
from libs.logger import logger
from os import listdir

log = logger.getLogger(__name__)

class Neo4jConnector:

    def __init__(self, uri, user, password, port=7687, tls=False, selfsigned=False):
        uri = f'{uri}:{port}'
        if tls and selfsigned:
            uri = f'bolt+ssc://{uri}'
        elif tls:
            uri = f'bolt+s://{uri}'
        else:
            uri = f'bolt://{uri}'

        self.driver = GraphDatabase.driver(uri, auth=(user, password))

    def close(self):
        """Closed Neo4j connection."""
        self.driver.close()

    @staticmethod
    def _query(tx, query):
        """Internal Neo4j query function."""
        result = tx.run(query)
        return list(result)

    def get(self, query):
        """Run Neo4j query that gets information from the database."""
        with self.driver.session() as session:
            result = session.read_transaction(self._query, query)
            return len(result)

    def change(self, query):
        """Run Neo4j query that changes the state of the database."""
        with self.driver.session() as session:
            result = session.write_transaction(self._query, query)
            return len(result)

def _update_node(conn, action, resource_type, resource_id, attributes):
    """Internal function that creates a valid query based on provided data."""
    if attributes:
        query = """%s (node:%s {id: '%s'})
        %s
        RETURN node""" % (action.upper(), resource_type, resource_id, attributes)
    else:
        query = """%s (node:%s {id: '%s'})
        RETURN node""" % (action.upper(), resource_type, resource_id)

    result = conn.change(query)

    return result

def _obj_to_set_attributes(obj):
    """Convert dict to Neo4j format."""
    result = []
    for key, attribute in obj.items():
        if "'" in str(attribute):
            attribute = '"{}"'.format(attribute)
        else:
            attribute = "'{}'".format(attribute)
        if not key and isinstance(attribute, str):
            key = attribute
        if not key:
            key = 'Empty'
        result.append(f"SET node.`{key}` = {attribute}")
        #result.append(f'SET node.`{key}` = "{attribute}"')
    return "\n".join(result)

def create_relationship(conn, node_a_type, node_a_id, node_b_type, node_b_id):
    """Create new relationship in Neo4j."""
    check_first_node = """
        MATCH (node:%s {id: '%s'})
        RETURN node
        """ % (node_a_type, node_a_id)
    node_a_result = conn.get(check_first_node)

    check_second_node = """
        MATCH (node:%s {id: '%s'})
        RETURN node
        """ % (node_b_type, node_b_id)
    node_b_result = conn.get(check_second_node)

    if not node_a_result:
        create_node(conn, node_a_type, node_a_id)
    if not node_b_result:
        create_node(conn, node_b_type, node_b_id)

    query = """MATCH (i:%s), (n:%s)
    WHERE i.id = '%s' AND n.id = '%s'
    MERGE (i)-[:RELATES_TO]->(n)
    RETURN i, n""" % (node_a_type, node_b_type, node_a_id, node_b_id)
    result = conn.change(query)
    return result

def create_node(conn, type, id, obj=None):
    """Create new node in Neo4j."""
    check_query = """
        MATCH (node:%s {id: '%s'})
        RETURN node
    """ % (type, id)

    attributes = None
    result = conn.get(check_query)

    if obj:
        attributes = _obj_to_set_attributes(obj)

    if result:
        # Update attributes of an existing node
        return _update_node(conn, 'MATCH', type, id, attributes)
    else:
        # Create new node with attributes
        return _update_node(conn, 'CREATE', type, id, attributes)

def create_relationships_from_links(conn, resource_name, resource_id, resource_links):
    link_count = 0
    threads = []

    for link in resource_links:
        link_count += 1
        log.info(f'Working on link {link_count}/{len(resource_links)}')
        for link_name, link_id in link.items():
            log.debug(f'Creating link from {resource_name}:{resource_id} to {link_name}:{link_id}')
            t = threading.Thread(target=create_relationship, args=(conn, resource_name, resource_id, link_name, link_id,))
            threads.append(t)
            t.start()

    # Start all threads
    #for t in threads:
    #    t.start()

    # Wait for all of them to finish
    for t in threads:
        t.join()

def get_resource_name(filename):
    """Get the resource being parsed based on filename."""

    filename_resource_part = ""
    filename_keyword = ""
    filename = filename.split('_')

    keywords = ['Describe', 'Get', 'List']

    for part in filename:
        for keyword in keywords:
            if keyword in part:
                filename_keyword = keyword
                filename_resource_part = part
                break

    if not filename_resource_part or not filename_keyword:
        return None, None

    resource = filename_resource_part.replace(filename_keyword, '')
    resource_singular = resource
    if resource[-1] == 's':
        resource_singular = resource[:-1]
    return resource, resource_singular

def link_normalizer(link, resource_name_singular, config):
    """Check if a link name should be converted or if a link should be ignored."""
    if not isinstance(link, dict):
        return

    for k, v in link.items():
        # Ignore link to self
        if k[:-2] == resource_name_singular:
            continue
        for rule in config['linkRules']['convertionRules']:
            if re.match(r'{}'.format(rule['match']), k):
                return
            if re.match(r'{}'.format(rule['match']), v):
                _result = {rule['resource']: v}
                if 'ignoreResource' in rule and resource_name_singular in rule['ignoreResource']:
                    return
                if 'action' in rule and rule['action'] == 'ignore':
                    return
                return {rule['resource']: v}
        else:
            #return {k[:-2]: v}
            #return {k.split('_')[0].replace('Id', ''): v}
            return {re.sub('Id$', '', re.sub('_Id$', '', k).split('_')[-1]): v}


def get_resource_name_id_mapping(resource_type, resource_key, resource_id, pattern, rules=None):
    """Get resources that have an ID pattern as a value."""

    if not re.match(r'[a-z]+-[a-f0-9]{,64}', resource_id):
        return None

    if rules:
        for rule in rules:
            if re.match(r'{}'.format(rule['match']), resource_id):
                if 'action' in rule and rule['action'] == 'ignore':
                    return None
                resource = rule['resource']
                resource_key = '{}Id'.format(rule['resource'])

    resource = re.sub(r'{}'.format(pattern), '', resource_key)
    resource = resource.split('_')[-1]

    if resource[-1] == 's':
        resource = resource[:-1]

    if rules:
        for rule in rules:
            if ('ignoreResource' in rule and
                resource == rule['resource'] and
                resource_type in rule['ignoreResource']):
                return None

    return {'resource': resource, 'key': resource_key, 'id': resource_id}

def create_resource_alias(object, key, alias, remove = False):
    """Duplicate a value with a different key."""
    _object = object.copy()

    if key in object:
        _object.update({alias: object[key]})

        if remove:
            _object.pop(key, None)

    return _object

def parseYaml(filename):
    """Read Yaml-file."""
    with open(filename, "r") as f:
        try:
            result = yaml.safe_load(f)
            return result
        except yaml.YAMLError as exc:
            log.error('Failed to parse YAML file {}'.format(filename))
            log.error(exc)

def get_json_files_in_dir(config, directory):
    """Fetch a list of files in specified directory."""
    files = []

    compiled_rules = None
    if 'files' in config and 'ignore' in config['files']:
        compiled_rules = '|'.join(config['files']['ignore'])
    for file in listdir(directory):
        if compiled_rules and re.match(compiled_rules, file):
            continue
        if isfile(join(directory, file)) and re.match(r'.*.json$', file) and file not in files:
            if file not in files:
                files.append(abspath(join(directory, file)))

    return files

def add_metadata_to_resource(config, resource, metadata):
    _resource = resource.copy()
    if not 'metadata' in config:
        log.warning('No metadata configured.')
        return

    if ('accountid' in config['metadata'] and
        config['metadata']['accountid'] and
        'accountid' in metadata):
        _resource.update({'_accountid': metadata['accountid']})

    if ('region' in config['metadata'] and
        config['metadata']['region'] and
        'region' in metadata):
        _resource.update({'_region': metadata['region']})

    if ('lastSeen' in config['metadata'] and
        config['metadata']['lastSeen'] and
        'lastSeen' in metadata):
        _resource.update({'_last_seen': metadata['lastSeen']})

    if ('aws_module' in config['metadata'] and
        config['metadata']['aws_module'] and
        'module' in metadata):
        _resource.update({'_aws_module': metadata['module']})

    return _resource
