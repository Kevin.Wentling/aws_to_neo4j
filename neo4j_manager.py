"""Script used to test a specific file and see what it would return."""

import sys
import json
import argparse

#from libs.helpers import helpers
from libs.helpers.helpers import (
    load_json, flatten_data, consolidate_fattened_data, remove_keys_from_dict_fuzzy, merge_dicts,
    find_keys, find_keys_with_pattern
)
from libs.logger import logger
import helpers

logger.disable()

log = logger.getLogger(__name__)

def get_resource(filename):
    """Load JSON object from file, return resource and results."""

    resource, resource_singluar = helpers.get_resource_name(filename)
    item = load_json(filename)
    results = find_keys(item, resource)[0]

    return resource, resource_singluar, results

if __name__ == '__main__':
    filename = sys.argv[1]
    #item = helpers.load_json(filename)

    parser = argparse.ArgumentParser(description='Fetch AWS metrics.')
    parser.add_argument('--filename', '-f', required=True,
                        help='File to parse')
    parser.add_argument('--all', '-a', action='store_true',
                        help='Return all information parsed.')
    parser.add_argument('--links', '-l', action='store_true',
                        help='Return all links parsed.')
    parser.add_argument('--resources', '-r', action='store_true',
                        help='Return all resources parsed.')
    parser.add_argument('--name', '-n', action='store_true',
                        help='Return name of parsed resource.')
    args = parser.parse_args()

    resource, resource_singular, results = get_resource(args.filename)

    if args.all or args.name:
        print("Resource name: {}".format(resource))
        print("Resource name singular: {}".format(resource_singular))

    config = helpers.parseYaml('config/config.yaml')

    for result in results:

        result = flatten_data(result)

        tags = consolidate_fattened_data(result, 'Tags', 'Key', 'Value')
        result = remove_keys_from_dict_fuzzy(result, 'Tags')

        result = merge_dicts(result, tags)
        ids = find_keys_with_pattern(result, '.*Id$')

        resource_links = []
        for _id in ids:
            response = helpers.get_resource_name_id_mapping(
                resource_singular,
                _id,
                result[_id],
                'Id$',
                config['linkRules']['convertionRules'])
            if response:
                resource_links.append(response)

        result = helpers.create_resource_alias(result, '{}Id'.format(resource_singular), 'id')

        if args.all or args.resources:
            print(json.dumps(result))

        if args.all or args.links:
            for resource_link in resource_links:
                print(resource_link)
