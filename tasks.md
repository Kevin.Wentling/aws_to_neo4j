# Tasks

- [x] Update nodes to include AWS region
- [ ] Update nodes to inckude AWS account ID
- [ ] Remove AWS account relationships
- [ ] Create file in AWS List all directory with owner information (owner.json)

# Issues

- [ ] sg-0e17b779ca9d82487 not linked properly to eni-0a345e2bbc58500a9, eni-0fe8e6bd0cd716e81, eni-034956e07b71dbbe8 (one example)
