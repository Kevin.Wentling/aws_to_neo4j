"""Unit tests for helpers."""

import unittest
import logging
import os

import helpers

from . import test_setup

# Disable logging for tests
logging.disable(logging.CRITICAL)

class TestConfigParser(test_setup.TestsSetup):
    """Functions to test functionality in config_parser."""

    def setUp(self):
        """Initial setup to run before every test."""
        super().setUp()

    def test_load_json_valid(self):
        self.assertEqual(1,1)

    def test_get_resource_name(self):
        expected_result_1 = "Instances"
        expected_result_2 = "Instance"
        resource, resource_singular = helpers.get_resource_name('ec2_DescribeInstances_eu-west-1_None.json')
        self.assertEqual(expected_result_1, resource)
        self.assertEqual(expected_result_2, resource_singular)

        expected_result_1 = "NetworkInterfaces"
        expected_result_2 = "NetworkInterface"
        resource, resource_singular = helpers.get_resource_name('ec2_DescribeNetworkInterfaces_eu-west-1_None.json')
        self.assertEqual(expected_result_1, resource)
        self.assertEqual(expected_result_2, resource_singular)

    def test_get_resource_name_invalid_filename(self):
        expected_result = (None, None)
        result = helpers.get_resource_name('something.json')
        self.assertEqual(expected_result, result)

    def test_get_resource_name_id_mapping(self):
        expected_result = {'resource': 'Vpc', 'key': 'response_NetworkInterfaces_849_VpcId', 'id': 'vpc-9274f3f7'}
        result = helpers.get_resource_name_id_mapping('Vpc', 'response_NetworkInterfaces_849_VpcId', 'vpc-9274f3f7', 'Id$')
        self.assertEqual(expected_result, result)

    def test_create_resource_alias(self):
        example_dict = {
            'item1': 'value1',
            'item2': 'value2'
        }
        expected_result = {
            'item1': 'value1',
            'item2': 'value2',
            'my_alias': 'value2'
        }
        result = helpers.create_resource_alias(example_dict, 'item2', 'my_alias')
        self.assertEqual(expected_result, result)

    def test_create_resource_alias_remove_key(self):
        example_dict = {
            'item1': 'value1',
            'item2': 'value2'
        }
        expected_result = {
            'item1': 'value1',
            'my_alias': 'value2'
        }
        result = helpers.create_resource_alias(example_dict, 'item2', 'my_alias', True)
        self.assertEqual(expected_result, result)

    def test_get_json_files_in_dir_none_removed(self):
        """Nothing should be removed if no file matches a rule."""
        directory = os.path.join(self.fixtures, 'json')
        config = {'files': {'ignore': ['.*ec2_DescribeVpcs.*']}}
        files_expected = [
            'ec2_DescribeInstanceStatus_eu-west-1_None.json',
            'ec2_DescribeTags_eu-west-1_None.json',
            'ec2_DescribeInstances_eu-west-1_None.json',
            'ec2_DescribeNetworkInterfacesStatus_eu-west-1_None.json'
        ]
        expected_result = []
        for file in files_expected:
            expected_result.append(os.path.join(self.fixtures, 'json', file))
        result = helpers.get_json_files_in_dir(config, directory)
        self.assertEqual(expected_result, result)

    def test_get_json_files_in_dir_one_removed(self):
        """One should be removed when only when is matching the rule."""
        directory = os.path.join(self.fixtures, 'json')
        config = {'files': {'ignore': ['.*ec2_DescribeTags.*']}}
        files_expected = [
            'ec2_DescribeInstanceStatus_eu-west-1_None.json',
            'ec2_DescribeInstances_eu-west-1_None.json',
            'ec2_DescribeNetworkInterfacesStatus_eu-west-1_None.json'
        ]
        expected_result = []
        for file in files_expected:
            expected_result.append(os.path.join(self.fixtures, 'json', file))
        result = helpers.get_json_files_in_dir(config, directory)
        self.assertEqual(expected_result, result)

    def test_get_json_files_in_dir_multiple_removed(self):
        """One should be removed when only when is matching the rule."""
        directory = os.path.join(self.fixtures, 'json')
        config = {'files': {'ignore': ['.*ec2_DescribeInstance.*']}}
        files_expected = [
            'ec2_DescribeTags_eu-west-1_None.json',
            'ec2_DescribeNetworkInterfacesStatus_eu-west-1_None.json'
        ]
        expected_result = []
        for file in files_expected:
            expected_result.append(os.path.join(self.fixtures, 'json', file))
        result = helpers.get_json_files_in_dir(config, directory)
        self.assertEqual(expected_result, result)

    def test_get_json_files_in_dir_multiple_removed_multipe_rules(self):
        """One should be removed when only when is matching the rule."""
        directory = os.path.join(self.fixtures, 'json')
        config = {'files': {'ignore': ['.*ec2_DescribeInstance.*', '.*Status.*']}}
        files_expected = [
            'ec2_DescribeTags_eu-west-1_None.json'
        ]
        expected_result = []
        for file in files_expected:
            expected_result.append(os.path.join(self.fixtures, 'json', file))
        result = helpers.get_json_files_in_dir(config, directory)
        self.assertEqual(expected_result, result)

    def test_get_json_files_in_dir_no_rules(self):
        """One should be removed when only when is matching the rule."""
        directory = os.path.join(self.fixtures, 'json')
        config = {}
        files_expected = [
            'ec2_DescribeInstanceStatus_eu-west-1_None.json',
            'ec2_DescribeTags_eu-west-1_None.json',
            'ec2_DescribeInstances_eu-west-1_None.json',
            'ec2_DescribeNetworkInterfacesStatus_eu-west-1_None.json'
        ]
        expected_result = []
        for file in files_expected:
            expected_result.append(os.path.join(self.fixtures, 'json', file))
        result = helpers.get_json_files_in_dir(config, directory)
        self.assertEqual(expected_result, result)

    def tearDown(self):
        pass

if __name__ == '__main__':
    unittest.main()
