"""Unit tests for helpers."""

import unittest
import logging
import os
import json

import aws_to_neo4j

from . import test_setup

# Disable logging for tests
logging.disable(logging.CRITICAL)

class TestConfigParser(test_setup.TestsSetup):
    """Functions to test functionality in config_parser."""

    def setUp(self):
        """Initial setup to run before every test."""
        super().setUp()
        self.config = aws_to_neo4j.loadConfig(os.path.join(self.fixtures, 'config.yaml'))

    def loadResultFile(self, filename):
        result_file = os.path.join(self.fixtures, 'aws_dumps_results', filename)
        with open(result_file, 'r') as f:
            expected_result = json.load(f)

        return expected_result

    def test_file_parse_DescribeInstancess(self):
        test_file = os.path.join(
            self.fixtures,
            'aws_dumps',
            'ec2_DescribeInstances_eu-west-1_None.json')
        expected_result = self.loadResultFile('ec2_DescribeInstances_eu-west-1_None.json')
        result = aws_to_neo4j.parseFile(self.config, test_file, 123456789102)
        self.assertEqual(result, expected_result)

    def test_file_parse_DescribeSecurityGroups(self):
        test_file = os.path.join(
            self.fixtures,
            'aws_dumps',
            'ec2_DescribeSecurityGroups_eu-west-1_None.json')
        expected_result = self.loadResultFile('ec2_DescribeSecurityGroups_eu-west-1_None.json')
        result = aws_to_neo4j.parseFile(self.config, test_file, 123456789012)
        self.assertEqual(result, expected_result)

    def test_file_parse_DescribeNetworkInterfaces(self):
        test_file = os.path.join(
            self.fixtures,
            'aws_dumps',
            'ec2_DescribeNetworkInterfaces_eu-west-1_None.json')
        expected_result = self.loadResultFile('ec2_DescribeNetworkInterfaces_eu-west-1_None.json')
        result = aws_to_neo4j.parseFile(self.config, test_file, 123456789012)
        self.assertEqual(result, expected_result)

    def test_file_parse_DescribeVpcs(self):
        test_file = os.path.join(
            self.fixtures,
            'aws_dumps',
            'ec2_DescribeVpcs_eu-west-1_None.json')
        expected_result = self.loadResultFile('ec2_DescribeVpcs_eu-west-1_None.json')
        result = aws_to_neo4j.parseFile(self.config, test_file, 123456789012)
        self.maxDiff = 5000
        self.assertEqual(result, expected_result)

    def tearDown(self):
        pass

if __name__ == '__main__':
    unittest.main()
