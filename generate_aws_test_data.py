import argparse
import json
import random

def load_json(file):
    try:
        with open(file, 'r') as f:
            data = json.load(f)
    except json.decoder.JSONDecodeError:
        print(f'{file} is not a JSON file.')
        return

    return data

def get_random_items(data, num_items=3):

    _data = data.copy()

    for k,_ in data['response'].items():
        if k not in ['ResponseMetadata']:
            randomlist = random.sample(range(1, len(data['response'][k])), num_items)
            _newlist = []
            for number in randomlist:
                _newlist.append(data['response'][k][number])
            _data['response'].pop(k, None)
            _data['response'].update({k: _newlist})
            break

    return _data


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Generate test data.')
    parser.add_argument('--file', '-f',
                        help='File to generate test data for.')
    args = parser.parse_args()

    data = load_json(args.file)
    data = get_random_items(data)

    print(json.dumps(data))
