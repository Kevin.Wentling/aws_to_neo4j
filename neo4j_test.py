import sys

from helpers import Neo4jConnector, create_node, create_relationship, get_json_files_in_dir

def neo4j_test():
    # bolt+ssc: Connect to bolt and accept self-signed certificates
    nc = Neo4jConnector("10.97.144.74",
                        "neo4j",
                        "oCnnYwwHgg5GNbB7RPXrYbraTibav2nelj4PbApkhLfraTBrQhvVHbOI5PD3ay0p",
                        tls=True,
                        selfsigned=True)
    # greeter.print_greeting("hello, world")

    QUERY = (
        "MATCH (n)"
        "RETURN n"
    )
    #greeter.query(QUERY)
    #greeter.create_relationship("")
    #QUERY = (
    #    "MATCH (p:Person) "
    #    "WHERE p.name = $person_name "
    #    "RETURN p.name AS name"
    #)

    #create_node(greeter, 'instance', 'i-45abd31')
    obj = {
        'size': 't3.small',
        '_status': 'offline',
        'kubernetes.io-cluster/eu1tst_eks001': '/.-.!"#%€%/(owned'
    }
    #create_node(nc, 'Instance', 'i-asdadasd', obj)
    #create_node(nc, 'Instance', 'i-33333', obj)
    #create_node(nc, 'Instance', 'i-44444', obj)
    create_node(nc, 'NetworkInterface', 'eni-4444444', obj)

    create_relationship(nc, 'Instance', 'i-33333', 'NetworkInterface', 'eni-4444444')
    create_relationship(nc, 'Instance', 'i-44444', 'NetworkInterface', 'eni-4444444')
    #create_relationship(nc, 'Instance', 'i-44444', 'NetworkInterface', 'eni-5555555')
    #create_relationship(nc, 'Instance', 'i-77777', 'NetworkInterface', 'eni-7777777')
    #print(_obj_to_set_attributes(obj))

    nc.close()

if __name__ == "__main__":
    neo4j_test()

    #files = get_json_files_in_dir(sys.argv[1])

    #for file in files:
    #    print(file)
