import argparse
import json
import logging
  
import aws_to_neo4j

# Disable logging
logging.disable(logging.CRITICAL)

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Generate test data results.')
    parser.add_argument('--filename', '-f', required=True,
                        help='File to generate result data for.')
    parser.add_argument('--config', '-c', default='config/config.yaml',
                        help='File to generate result data for.')
    args = parser.parse_args()

    config = aws_to_neo4j.loadConfig(args.config)

    result = aws_to_neo4j.parseFile(config, args.filename)

    print(json.dumps(result))
