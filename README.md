aws\_to\_neo4j
=====

## Clone project

```
% git clone --recursive ****
```

## Tests

Run unit tests:

```
% python -m unittest discover
```


## Update

Update the submodules

```
% git submodule update --remote
```

## Notes
There is a setting in Neo4j limiting the number of nodes and relationships returned for a query.

### Get all nodes
```
MATCH (n) RETURN n
```

### Get nodes except
```
MATCH (n) WHERE NOT n:BlockDeviceMapping RETURN n
```

### Get nodes with regex
```
MATCH (n)-[]->(m)
WHERE n.Name =~ 'eu1tst.*'
RETURN n,m
```

### Delete x nodes
```
MATCH (n) WITH n LIMIT 1000 DETACH DELETE n
```